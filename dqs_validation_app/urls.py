from django.conf.urls import patterns, include, url
import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
    url(r'^login', views.user_login),
    url(r'^logout', views.user_logout),
    url(r'^upload', views.upload, name = 'jfu_upload' ),
    url(r'^checkvalidationstatus', views.checkvalidationstatus),
    url(r'^fileid', views.fileid),
    url(r'^getfile',views.getfile),
    url(r'^$', views.main, name="main")
)

#remove this in production and use a web server to serves statics
urlpatterns += staticfiles_urlpatterns()
