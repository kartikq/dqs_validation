# tasks.py (in one of your apps)
from celery import task
from validator import validate

@task()
def validate_file(validation_task):
  validate(validation_task)
