from dqsvalidation.xml_validator import XMLValidator
from dqsvalidation.csv_validator import CSVValidator
from django.utils import timezone
import traceback

def validate(validation_task):
  validation_task.log_location = validation_task.file_location + ".log.txt"
  try:
    validator = getValidator(validation_task)
    validator.process()
    validation_task.is_complete = True
    validation_task.has_errors = False 
  except:
    validation_task.has_errors = True
    validation_task.errors = traceback.format_exc()
  finally:
    validation_task.timestamp_end = timezone.now()
    validation_task.save()

def getValidator(validation_task):
  if validation_task.file_type == 'text/xml':
    return XMLValidator(file_to_validate = validation_task.file_location)
  elif validation_task.file_type == 'text/csv':
    return CSVValidator(file_to_validate = validation_task.file_location)
  else:
    raise Exception("Wrong file type detected. Only Xml and Csv files are currently supported")