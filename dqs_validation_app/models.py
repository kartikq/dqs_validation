from django.db import models

class ValidationTask(models.Model):
  """
  Validation task entry. Used for checking the status of a task
  """
  timestamp_start = models.DateTimeField(auto_now_add = True)
  timestamp_end = models.DateTimeField(auto_now_add = True)
  file_id = models.CharField(blank = False, max_length = 40)
  file_type = models.CharField(max_length = 100)
  file_location = models.TextField(blank = True)
  log_location = models.TextField(blank = True)
  is_complete = models.BooleanField(default = False)
  has_errors = models.BooleanField(default = False)
  errors = models.TextField(blank = True)
  username = models.CharField(max_length = 200)
