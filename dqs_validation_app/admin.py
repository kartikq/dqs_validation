from django.contrib import admin
from dqs_validation_app.models import ValidationTask

class ValidationTaskAdmin(admin.ModelAdmin):
  readonly_fields = ('timestamp_start',)
  list_display = ('username', 'timestamp_start', 'timestamp_end', 'duration', 'file_id', 'is_complete', 'has_errors')

  def duration(self, obj):
    return str( (obj.timestamp_end - obj.timestamp_start).seconds )
  duration.short_description = 'Duration (seconds)'

admin.site.register(ValidationTask, ValidationTaskAdmin)