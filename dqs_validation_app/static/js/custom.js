$(function(){

	$('input, textarea').placeholder();
	
	$('input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_square-purple',
		increaseArea: '20%'
	});

	$('.dv-table thead .icheckbox_square-purple input').on('ifChecked', function(){
		var $this = $(this);
		$this.closest('.dv-table').find('tbody .icheckbox_square-purple input').iCheck('check');
				
		viewDelete();
	});
	$('.dv-table thead .icheckbox_square-purple input').on('ifUnchecked', function(){
		var $this = $(this);
		$this.closest('.dv-table')
			.find('tbody .icheckbox_square-purple input').iCheck('uncheck');
		viewDelete();
	});


	$('.dv-table tbody .icheckbox_square-purple').on('ifClicked', function() {
		viewDelete();
	});



	var viewDelete = function() {
		window.setTimeout(function(){
			var checkCount = $('.dv-table tbody .checked').length;
			$('.checkCounter').text(checkCount);


			if( checkCount > 0 ) {
				$('.dv-msg-action td')
					.stop()
						.animate({
							height: 50
						});
			} else {
				$('.dv-table thead .icheckbox_square-purple input').iCheck('uncheck');
				$('.dv-msg-action td')
					.stop()
						.animate({
							height: 0
						});
			}
		}, 100);
	};
	
});