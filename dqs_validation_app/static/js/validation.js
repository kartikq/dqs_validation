/*
* JS for validation app
*/
$(function(){
  $.jStorage.flush();
  window.setInterval(function() {
    if(typeof $.jStorage.index() != 'undefined' && $.jStorage.index().length>0) {
      $.get('checkvalidationstatus', { 'fileids': $.jStorage.index() }).done( function(data) {
        $.each(data.fileidsprocessed, function(id, fileid){
          $('#file'+fileid).html('<a href="getfile?file_id='+ fileid +'" onclick="window.open(\'getfile?file_id=' + fileid +'\', \'newwindow\', \'width=300, height=250\'); return false;" class="btn btn-default dv-download-file"><span class="glyphicon glyphicon-download-alt"></span> Download Report</a>');
          $.jStorage.deleteKey(fileid);
        });
        $.each(data.fileidsinerror, function(id, fileid) {
          $('#file'+fileid).html('<span class="glyphicon glyphicon-remove"></span> Error processing file');  
          $.jStorage.deleteKey(fileid);
        });
      });
    }
  }, 10000);
  
  //file upload control 
  $("#upload").fileupload({
    dataType:'json',
    add: function(e, data) {
      $.each(data.files, function (index, file) {  
        var fileid
        $.ajax({
          url:'fileid',
          success: function(resp){
                    fileid = resp.fileid;
                    $('#fileid').val(fileid);
                  },
          async: false
        });
        
        var filename = '<td>'+file.name+'</td>';
        var status = '<td><div id="file' + fileid + '"></div></td>';
        $('#uploadstatus tr:last').after('<tr>'+filename+status+'</tr>');
      });
      data.submit();
    },
    progress: function (e, data) {
      $.each(data.files, function (index, file) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress-bar').css(
            'width',
            progress + '%'
        );
      });
    },
    done: function(e, data) {
      $.each(data.result.files, function (index, file) {
        $('#file'+file.file_id).html('<img src="static/ajax-loader.gif" alt="loading"/>');
        //update local storage with file id to be processed
        $.jStorage.set(file.file_id, 'true');
      });
    }
  });
});