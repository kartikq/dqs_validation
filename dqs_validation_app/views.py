from django.shortcuts import render, render_to_response
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseNotFound
from django.template import RequestContext
from app_settings import VALIDATION_FILE_UPLOAD_BASE_PATH
import os
from tasks import validate_file
from datetime import datetime
import uuid
import json
from jfu.http import UploadResponse
from models import ValidationTask
from sendfile import sendfile

@login_required
def main(request):
    context = RequestContext(request)
    return render_to_response('main.html', {}, context)

@login_required
def checkvalidationstatus(request):
    '''
    Check whether given file ids have been validated
    '''
    response_data = {}
    if request.method == 'GET':
        fileids = request.GET.getlist('fileids[]')
        fileids_processed = [ task.file_id for task in ValidationTask.objects.filter(file_id__in=fileids).filter(is_complete=True)]
        fileids_inerror = [ task.file_id for task in ValidationTask.objects.filter(file_id__in=fileids).filter(has_errors=True)]
        response_data = { 'fileidsprocessed' : fileids_processed, 'fileidsinerror': fileids_inerror }
    return HttpResponse(json.dumps(response_data), content_type="application/json")

@login_required
def fileid(request):
  if request.method == 'GET':
    fileid = str(uuid.uuid1())
    validation_task = ValidationTask(file_id=fileid);
    validation_task.save()
    response_data = {'fileid': fileid}
    return HttpResponse(json.dumps(response_data), content_type ="application/json")

@login_required
def getfile(request):
  if request.method == 'GET':
    fileid = request.GET.get('file_id')
    if(fileid):
      task = ValidationTask.objects.filter(file_id=fileid)[0]
      return sendfile(request, task.log_location)
    else:
      return HttpResponseNotFound('<h1>File not found</h1>')

@login_required
@require_POST
def upload(request):
    response_data = {}
    fid = request.POST['fileid']
    file = request.FILES['fileupload']
    handle_uploaded_file(file, fid, request.user.username)
    response_data = { 'name' : file.name,
        'size': file.size, 'file_id': fid }
    return UploadResponse( request, response_data )
    
def handle_uploaded_file(f, fid, username):
    fname = os.path.join(VALIDATION_FILE_UPLOAD_BASE_PATH, fid)
    with open(fname, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
            
    validation_task = update_validation_task(fid, fname, f.content_type, username)
    validate_file.delay(validation_task)

def update_validation_task(fid, f, ftype, username):
  validation_task = ValidationTask.objects.filter(file_id=fid)[0]
  if validation_task:
    validation_task.file_location = f
    validation_task.file_type = ftype
    validation_task.username = username
    validation_task.save()
    return validation_task
  else:
    return None
    
@login_required
def user_logout(request):
    # Since we know the user is logged in, we can now just log them out.
    logout(request)

    # Take the user back to the homepage.
    return HttpResponseRedirect('/')

def user_login(request):
    context = RequestContext(request)

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                # If the account is valid and active, we can log the user in.
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                # An inactive account was used - no logging in!
                return HttpResponse("Your account is disabled.")
        else:
                # Bad login details were provided. So we can't log the user in.
            return HttpResponse("Invalid login details supplied.")

    else:
        return render_to_response('login.html', {}, context)
