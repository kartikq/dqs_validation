from django.conf import settings

VALIDATION_FILE_UPLOAD_BASE_PATH = getattr(settings, 'VALIDATION_FILE_UPLOAD_BASE_PATH', '/tmp')